let numberArr = [];
let realNumberArr = [];

// Thêm số n
function themSo() {
  let number = document.querySelector(`#txt-number`).value * 1;
  numberArr.push(number);
  document.querySelector(`#txt-number`).value = ``;
  document.getElementById(`result-n`).innerHTML = numberArr;
  return numberArr;
}
function themSoThuc() {
  let n = document.querySelector(`#txt-num`).value * 1;
  realNumberArr.push(n);
  document.querySelector(`#txt-num`).value = ``;
  document.getElementById(`result-num`).innerHTML = realNumberArr;
  return realNumberArr;
}

// Bài 1
function tinhTong() {
  let a = numberArr;
  let tong = 0;
  for (let i = 0; i < a.length; i++) {
    if (a[i] > 0) {
      tong += a[i];
    }
  }
  document.getElementById(`result-1`).innerHTML = `Tổng số dương: ${tong}`;
}

// Bài 2
function demSoDuong() {
  let b = numberArr;
  let countSoDuong = 0;
  for (let i = 0; i < b.length; i++) {
    if (b[i] > 0) {
      countSoDuong++;
    }
  }
  document.getElementById(`result-2`).innerHTML = `Số dương: ${countSoDuong}`;
}

// Bài 3
function timSoNhoNhat() {
  let c = numberArr;
  let min = c[0];
  for (let i = 0; i < c.length; i++) {
    if (min > c[i]) {
      min = c[i];
    }
  }
  document.getElementById(`result-3`).innerHTML = `Số nhỏ nhất: ${min}`;
}

// Bài 4
function soDuongNhoNhat() {
  let mangSoDuong = [];
  let d = numberArr;

  for (let i = 0; i < d.length; i++) {
    if (d[i] > 0) {
      mangSoDuong.push(d[i]);
    }
  }
  if (mangSoDuong.length == 0) {
    document.getElementById(
      `result-4`
    ).innerHTML = `Không có số dương nhỏ nhất`;
  } else {
    let minSoDuong = mangSoDuong[0];
    for (let j = 0; j < mangSoDuong.length; j++) {
      if (minSoDuong > mangSoDuong[j]) {
        minSoDuong = mangSoDuong[j];
      }
    }
    document.getElementById(
      `result-4`
    ).innerHTML = `Số dương nhỏ nhất: ${minSoDuong}`;
  }
}

// Bài 5
function soChanCuoiCung() {
  let lastEvenNumber = 0;
  let e = numberArr;

  for (let i = 0; i < e.length; i++) {
    if (e[i] % 2 == 0) lastEvenNumber = e[i];
  }
  document.getElementById(
    `result-5`
  ).innerHTML = `Số chẵn cuối cùng trong mảng: ${lastEvenNumber}`;
}

// Bài 6
function doiCho() {
  let f = numberArr;
  let temp = 0;
  let vitri1 = document.getElementById(`vitri-1`).value * 1 - 1;
  let vitri2 = document.getElementById(`vitri-2`).value * 1 - 1;
  temp = f[vitri1];
  f[vitri1] = f[vitri2];
  f[vitri2] = temp;
  document.getElementById(`result-6`).innerHTML = `Mảng sau khi đổi: ${f}`;
}

// Bài 7
function sapXepTangDan() {
  let g = numberArr;
  for (let i = 0; i < g.length - 1; i++) {
    for (let j = g.length - 1; j > i; j--) {
      if (g[j] < g[j - 1]) {
        let t = g[j];
        g[j] = g[j - 1];
        g[j - 1] = t;
      }
    }
  }
  document.getElementById(
    `result-7`
  ).innerHTML = `Mảng sau khi đã sắp xếp: ${g}`;
}

// Bài 8
function isPrime(num) {
  if (num < 2) return false;
  for (let i = 2; i < num; i++) {
    if (num % i == 0) return false;
  }
  return true;
}
let h = numberArr;
function timSoNguyenTo() {
  for (let i = 0; i < h.length; i++) {
    if (isPrime(h[i])) {
      document.getElementById(
        `result-8`
      ).innerHTML = `Số nguyên tố đầu tiên: ${h[i]}`;
      break;
    }
  }
  document.getElementById(
    `result-8`
  ).innerHTML = `Không có số nguyên tố trong mảng`;
}

// Bài 9
function demSoNguyen() {
  let h = realNumberArr;
  let countSoNguyen = 0;
  for (let i = 0; i < h.length; i++) {
    if (Number.isInteger(h[i])) {
      countSoNguyen++;
    }
  }
  document.getElementById(
    `result-9`
  ).innerHTML = `Số nguyên có trong mảng là: ${countSoNguyen}`;
}

// Bài 10
function soSanh() {
  p = numberArr;
  let soDuong = 0;
  let soAm = 0;
  for (let i = 0; i < p.length; i++) {
    if (p[i] > 0) {
      soDuong++;
    } else {
      soAm++;
    }
  }
  if (soDuong > soAm) {
    document.getElementById(`result-10`).innerHTML = `Số dương > số âm`;
  } else {
    document.getElementById(`result-10`).innerHTML = `Số dương < số âm`;
  }
}
